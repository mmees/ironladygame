﻿using UnityEngine;
using System.Collections;

public class PlayerAnimScript : MonoBehaviour {

    OTAnimatingSprite mySprite;
    HelperScript.anim currentAnim;

	// Use this for initialization
	void Start () 
    {
        // get my sprite
        mySprite = GetComponent<OTAnimatingSprite>();
        mySprite.Play("StandL");
	}
	
	void Update () 
    {
	    //running left
        if (PlayerScript.leftKey && !PlayerScript.isOnLadder && !PlayerScript.isFalling && (currentAnim != HelperScript.anim.WalkLeft))
        {
            currentAnim = HelperScript.anim.WalkLeft;
            mySprite.Play("RunL");
        }
        if (!PlayerScript.leftKey && !PlayerScript.isOnLadder && !PlayerScript.isFalling && (currentAnim != HelperScript.anim.StandLeft) && PlayerScript.facing == 1)
        {
            currentAnim = HelperScript.anim.StandLeft;
            mySprite.Play("StandL");
        }

        //running right
        if (PlayerScript.rightKey && !PlayerScript.isOnLadder && !PlayerScript.isFalling && (currentAnim != HelperScript.anim.WalkRight))
        {
            currentAnim = HelperScript.anim.WalkRight;
            mySprite.Play("RunR");
        }
        if (!PlayerScript.rightKey && !PlayerScript.isClimbingLadder && !PlayerScript.isFalling && (currentAnim != HelperScript.anim.StandRight) && PlayerScript.facing == 2)
        {
            currentAnim = HelperScript.anim.StandRight;
            mySprite.Play("StandR");
        }
       
        // climb
        if (PlayerScript.upKey && PlayerScript.isClimbingLadder && PlayerScript.isOnLadder && currentAnim != HelperScript.anim.Climb)
		{
            currentAnim = HelperScript.anim.Climb;
			mySprite.Play("climb");
		}
        if (!PlayerScript.upKey && PlayerScript.isOnLadder && currentAnim != HelperScript.anim.ClimbStop && PlayerScript.facing == 3)
		{
            currentAnim = HelperScript.anim.ClimbStop;
			mySprite.ShowFrame(1); // climb left
		}

        if (PlayerScript.downKey && PlayerScript.isClimbingLadder && PlayerScript.isOnLadder && currentAnim != HelperScript.anim.Climb)
		{
            currentAnim = HelperScript.anim.Climb;
			mySprite.Play("Climb");
		}
        if (!PlayerScript.downKey && PlayerScript.isOnLadder && currentAnim != HelperScript.anim.ClimbStop && PlayerScript.facing == 4)
		{
            currentAnim = HelperScript.anim.ClimbStop;
			mySprite.ShowFrame(1); // climb left
		}
        // falling
        if (PlayerScript.isFalling && currentAnim != HelperScript.anim.FallLeft && PlayerScript.facing == 1)
        {
            currentAnim = HelperScript.anim.FallLeft;
            mySprite.Play("FallL"); // fall left
        }
        if (PlayerScript.isFalling && currentAnim != HelperScript.anim.FallRight && PlayerScript.facing == 2)
        {
            currentAnim = HelperScript.anim.FallRight;
            mySprite.Play("FallR"); // fall right
        }
	}
}
