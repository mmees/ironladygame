﻿using UnityEngine;
using System.Collections;

public class TrapBrickScript : MonoBehaviour {

    public float distanceToGround;
    private bool isMoving;
    private float distanceTravelled;
    OTAnimatingSprite mySprite;

	// Use this for initialization
	void Start () 
	{
        isMoving = false;
        distanceTravelled = 0.0f;
        mySprite = GetComponent<OTAnimatingSprite>();
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (isMoving && distanceTravelled < distanceToGround)
        {
            Vector3 trans = new Vector3(0.0f, -4.0f, 0.0f);
            trans *= Time.deltaTime;
            transform.Translate(trans);
            distanceTravelled += -trans.y;
        }
	}

    public void Triggered()
    {
        Debug.Log("I've been triggered!");
        isMoving = true;
    }   

    public bool GetIsMoving()
    {
        return isMoving;
    }
}
