﻿using UnityEngine;
using System.Collections;

public class TrapBrickColliderActivationScript : MonoBehaviour 
{
    public TrapBrickScript child;
    void OnTriggerEnter(Collider otherObject)
    {
        if (otherObject.CompareTag("Player") )
        {
            if (!child.GetIsMoving())
            {
                //activate the trap!
                child.Triggered();
            }
        }
    }    
}
