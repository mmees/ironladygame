﻿using UnityEngine;
using System.Collections;

public class FallingPlatformScript : MonoBehaviour
{

    public float distanceToGround;
    private bool isMoving;
    private float distanceTravelled;
    private bool TimerExpired;
    public float StandTimeLength = 0.05f;
    public float RespawnTimeLength = 1.0f;
    private float timer;
    private float respawnTimer;
    private bool IsDead;
    OTSprite mySprite;
    private Vector3 spawnPoint;
    public bool CanRespawn = true;



	// Use this for initialization
	void Start () 
	{
        isMoving = false;
        IsDead = false;
        distanceTravelled = 0.0f;
        mySprite = GetComponent<OTSprite>();
        spawnPoint = transform.position;
        if (!CanRespawn)
        {
            mySprite.frameName = "zBlockFallingPermanent";
        }
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (isMoving && !TimerExpired)
        {
            timer += Time.deltaTime;
            if (timer >= StandTimeLength)
            {
                TimerExpired = true;
            }
        }

        if (isMoving && distanceTravelled < distanceToGround && TimerExpired)
        {
            Vector3 trans = new Vector3(0.0f, -4.0f, 0.0f);
            trans *= Time.deltaTime;
            transform.Translate(trans);
            distanceTravelled += -trans.y;
        }

        if (isMoving && distanceTravelled >= distanceToGround)
        {
            distanceTravelled = 0.0f;
            isMoving = false;
            this.gameObject.collider.enabled = false;
            this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            IsDead = true;
        }
        if (CanRespawn)
        {
            if (!isMoving && IsDead && respawnTimer <= RespawnTimeLength)
            {
                respawnTimer += Time.deltaTime;
            }

            if (!isMoving && IsDead && respawnTimer > RespawnTimeLength)
            {
                IsDead = false;
                TimerExpired = false;
                this.gameObject.collider.enabled = true;
                this.gameObject.GetComponent<MeshRenderer>().enabled = true;
                transform.position = spawnPoint;
                respawnTimer = 0.0f;
                timer = 0.0f;
            }
        }
	}

    public void Triggered()
    {
        isMoving = true;
    }   

    public bool GetIsMoving()
    {
        return isMoving;
    }
}
