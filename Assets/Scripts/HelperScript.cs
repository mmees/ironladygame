﻿using UnityEngine;
using System.Collections;

public class HelperScript : MonoBehaviour {

    public static float CameraSize;
    public static float CameraSizeX;
    public static float CameraAspectRatio;

    public GUIText ScoreCounter;
    public GUIText DynamiteCounter;

    [HideInInspector]
    public static int score;
    public static int dynamiteAmount;
    [HideInInspector]
    public enum anim { None, WalkLeft, WalkRight, Climb, ClimbStop, StandLeft, StandRight, FallLeft, FallRight, ShootLeft, ShootRight }
    [HideInInspector]
    public enum movementDirections { Left, Right, Up, Down };
   

	// Use this for initialization
	void Start () 
    {
        CameraAspectRatio = Camera.main.aspect;
        CameraSize = Camera.main.orthographicSize;
        CameraSizeX = CameraSize * CameraAspectRatio;
	}
	

	void LateUpdate () 
    {
	    //update score
        ScoreCounter.text = score.ToString();
        DynamiteCounter.text = dynamiteAmount.ToString();
	}
}
