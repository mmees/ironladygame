﻿using UnityEngine;
using System.Collections;

public class FallingPlatformColliderActivationScript : MonoBehaviour 
{
    public FallingPlatformScript child;
    void OnTriggerEnter(Collider otherObject)
    {
        if (otherObject.CompareTag("Player") )
        {
            if (!child.GetIsMoving())
            {
                //activate the trap!
                child.Triggered();
            }
        }
    }    
}
