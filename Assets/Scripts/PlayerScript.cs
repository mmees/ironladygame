﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {
    
    //player variables
    //----------------

    //adjustables
    public float moveSpeed = 5f;
    public float gravity = 0.00981f;

    public GameObject dynamitePlacedPrefab;

    //movement variables
    private Vector3 spawnPoint;
    private int moveDirX;
    private int moveDirY;
    private Vector3 movement;
    private Transform thisTransform;

    public static int facing;

    //collision raycast stuff
    private float rayBlockedDistX = 0.4f;
    private RaycastHit hit;

    private float playerHitboxX = 0.225f;
    private float playerHitboxY = 0.5f;

    private bool blockedLeft;
    private bool blockedRight;
    private bool blockedUp;
    private bool blockedDown;

    public LayerMask groundMask;

    public static Vector3 glx;

    //player state
    public static bool isAlive;
    public static bool isOnLadder;
    public static bool isFalling;
    public static bool isClimbingLadder;

    //key variables
    public static bool leftKey;
    public static bool rightKey;
    public static bool upKey;
    public static bool downKey;
    public static bool fireKey;

    public static float stickAmountX;
    public static float stickAmountY;

    //code

    void Awake()
    {
        thisTransform = transform;
    }

    void Start()
    {
        spawnPoint = thisTransform.position; // player will respawn at initial starting point
        isAlive = true;
        facing = 1;
    }

	// Update is called once per frame
	void Update () 
    {
        //update inputs
        getInput();

        //perform raycasts for collision
        DoCollisionRaycasts();

        moveDirX = 0;
        moveDirY = 0;

        isClimbingLadder = false;

        // move left
        if (leftKey && !blockedLeft)
        {
            moveDirX = -1;
            facing = 1;
        }

        // move right
        if (rightKey && !blockedRight)
        {
            moveDirX = 1;
            facing = 2;
        }

        // move up (ladder)
        if (upKey && isOnLadder )
        {
            moveDirY = 1;
            facing = 3;
            isClimbingLadder = true;
        }

        // move down (ladder)
        if (downKey && isOnLadder)
        {
            moveDirY = -1;
            facing = 4;
            isClimbingLadder = true;
        }

        // now perform movement;
        UpdateMovement();
	}

    void getInput()
    {
        stickAmountX = 0.0f;
        stickAmountY = 0.0f;
        leftKey = rightKey = upKey = downKey = false;

        if (Input.GetAxis("Horizontal") > 0.1f)
        {
            rightKey = true;
            stickAmountX = Mathf.Abs(Input.GetAxis("Horizontal"));
        }
        if (Input.GetAxis("Horizontal") < -0.1f)
        {
            leftKey = true;
            stickAmountX = Mathf.Abs(Input.GetAxis("Horizontal"));
        }

        if (Input.GetAxis("Vertical") > 0.1f)
        {
            upKey = true;
            stickAmountY = Mathf.Abs(Input.GetAxis("Vertical"));
        }
        if (Input.GetAxis("Vertical") < -0.1f)
        {
            downKey = true;
            stickAmountY = Mathf.Abs(Input.GetAxis("Vertical"));
        }

        if (Input.GetButton("Fire") && HelperScript.dynamiteAmount > 0)
        {
            --HelperScript.dynamiteAmount;
            GameObject instantiateddynamite = (GameObject)Instantiate(dynamitePlacedPrefab);
        }
       
    }

    void UpdateMovement()
    {
        if (!isFalling || isOnLadder)
        {
            movement = new Vector3(moveDirX * stickAmountX, moveDirY * stickAmountY, 0);
            movement *= Time.deltaTime * moveSpeed;
            transform.Translate(movement);
        }
        else
        {
            movement = new Vector3(0.0f, -4.0f, 0f);
            movement *= Time.deltaTime;
            transform.Translate(movement);
        }
    }

    void DoCollisionRaycasts()
    {
        blockedLeft = blockedRight = false;

        RaycastHit output;
        // am I stood on the ground?
        if (Physics.Raycast(new Vector3(thisTransform.position.x - 0.3f, thisTransform.position.y, thisTransform.position.z + 1.0f), Vector3.down, out output, 0.5f, groundMask) || Physics.Raycast(new Vector3(thisTransform.position.x + 0.3f, thisTransform.position.y, thisTransform.position.z + 1.0f), Vector3.down, out output, 0.5f, groundMask))
        {
            isFalling = false;
        }
        else
        {
            if (!isOnLadder && !isFalling)
            {
                isFalling = true;
            }
        }

        // are we being blocked on the right? cast two rays, one head height, one feet height.
        if (Physics.Raycast(new Vector3(thisTransform.position.x, thisTransform.position.y + 0.3f, thisTransform.position.z + 1f), Vector3.right, out output, rayBlockedDistX, groundMask) || Physics.Raycast(new Vector3(thisTransform.position.x, thisTransform.position.y - 0.4f, thisTransform.position.z + 1f), Vector3.right, out output, rayBlockedDistX, groundMask))
        {
            Debug.Log("Collided with something to the right.");
            blockedRight = true;
        }

        // are we blocked on the left?
        if (Physics.Raycast(new Vector3(thisTransform.position.x, thisTransform.position.y + 0.3f, thisTransform.position.z + 1f), -Vector3.right, out output, rayBlockedDistX, groundMask) || Physics.Raycast(new Vector3(thisTransform.position.x, thisTransform.position.y - 0.4f, thisTransform.position.z + 1f), -Vector3.right, out output, rayBlockedDistX, groundMask))
        {
            Debug.Log("Collided with something to the left.");
            blockedLeft = true;
        }

        // Am I on the far right edge of the screen?
        if (transform.position.x + playerHitboxX > (Camera.main.transform.position.x + HelperScript.CameraSizeX))
        {
            blockedRight = true;
        }

        // Or am I on the left?
        if (transform.position.x - playerHitboxX < (Camera.main.transform.position.x - HelperScript.CameraSizeX))
        {
            blockedLeft = true;
        }
    }

    // for later
    void RespawnPlayer()
    {
        // respawn the player at her initial start point
        transform.position = spawnPoint;
        isAlive = true;
    }

    void OnTriggerStay(Collider otherObject)
    {

        // Am I touching a ladder?
        if (otherObject.CompareTag("Ladder"))
        {
            Vector3 ladderHitbox;
            isOnLadder = false;
            blockedUp = false;
            blockedDown = false;

            ladderHitbox.y = otherObject.transform.localScale.y * 0.5f; // get half the ladders Y height

            // is the player overlapping the ladder?
			// if player is landing on top of ladder from a fall, let him pass by
			if ((transform.position.y + playerHitboxY) < ((ladderHitbox.y + 0.1f) + otherObject.transform.position.y))
			{
				isOnLadder = true;
				isFalling = false;
			}

            // if I'm at the top, snap me to the top.
            if ((transform.position.y + playerHitboxY) >= (ladderHitbox.y + otherObject.transform.position.y) && upKey)
			{
                glx = thisTransform.position;
                glx.y = (ladderHitbox.y + otherObject.transform.position.y) - playerHitboxY;
                thisTransform.position = glx;
				blockedUp = true;
			}

            // if we're at the bottom, snap us there

            if ((thisTransform.position.y - playerHitboxY) <= (-ladderHitbox.y + otherObject.transform.position.y))
            {
                glx = thisTransform.position;
                glx.y = (-ladderHitbox.y + otherObject.transform.position.y) + playerHitboxY;
                thisTransform.position = glx;
                blockedDown = true;
            }

        }
        if (otherObject.CompareTag("ScorePowerup"))
        {
            HelperScript.score += 250;
            Destroy(otherObject.gameObject);
        }
        if (otherObject.CompareTag("DynamitePowerup"))
        {
            HelperScript.score += 100;
            HelperScript.dynamiteAmount++;
            Destroy(otherObject.gameObject);
        }
    }

    void OnTriggerExit(Collider otherObject)
    {

        // did the player exit a ladder trigger?
        if (otherObject.gameObject.CompareTag("Ladder"))
        {
            isOnLadder = false;

        }
    }
    
}
